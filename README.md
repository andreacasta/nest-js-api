# nest-js-api

> Note: all'interno dell repository c'è il file `Insomnia_2022-07-06.json` per importare la configurazione delle chiamate api su Insomnia.

> ho usato YARN come gestore di paccheti.

## Installazione
### Run the API in development mode
```sh
yarn 
yarn db:dev:restart 
yarn start:dev 
npx prisma studio 
```
>  prisma studio server per interfacciarsi in modo grafico con il database postgres creato in docker 

### Run the API in test mode
```sh
yarn 
yarn db:test:restart 
yarn test:e2e 
npx prisma studio 
```
